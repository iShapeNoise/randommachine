# RandomMachine

![](images/RandomMachine_1.JPG)

RandomMachine is a device, that uses uranium glass and a Geiger counter to create a true random generator. The idea comes from [Jayson](https://http://www.jaysonhaebich.com/) a student friend of mine. I re-created this idea in a compact form with extended use for FoxDot (Musical live-coding environment). The Arduino sends time delays between each measured knocked-off electron (ionization). This delay can be used e.g. as duration of a note played by a synth in FoxDot.

![](images/RandomMachine_2.JPG)

![Video in /images](images/RandomMachine.mp4)


### RandomMachine used with FoxDot

![Video in /images](images/RandomMachineWithFoxDot.mp4)

<br>

| 1. Components      |
| :------------- |
| ![](images/RandomMachine_3.JPG) |

<br>

- 1 x Arduino Micro
- 1 x MAX9814 Microphone Sound sensor
- 1 x DIY Geiger Counter Parts Kit Modul Nuclear Radiation Detector F3K3
- 1 x On/Off switch
- Wires
- 1 x Piece of Uranium glass

| 2. Print Case Parts  (Scale parts by 2% to include shrinking rate)   |
| :------------- |
| ![](images/RandomMachineCase_1.png) |
|  |
| ![](images/RandomMachineCase_2.png) |

<br>

| 3. Assembling     |
| :------------- |
| ![](images/RandomMachine_Sketch.png) |

<br>

1. Assemble D.I.Y. Kit of Geiger counter

    ***Do not solder audio jack socket, Piezo buzzer, and D23 LED onto board!***

    *Optional: Replace switch of kit with a bigger version. In this case do not solder the switch to the board either*

2. Use wires between Piezo buzzer and glue microphone module and piezo buzzer together

3. Drill holes in case to connect wires
4. Use wires between Geiger Board (D 23) and LED light glued into case
5. Geiger Counter Breakout connector +/- >> Arduino 5V/GND
6. Sound Sensor AUD >> Arduino A2
7. Sound Sensor + >> Arduino 5V
8. Sound Sensor - >> Arduino GND
9. Connect Arduino to Computer and upload randomMachine.ino

<br>

## How does it work?

- Connect device to your laptop/PC and switch it on
- Use case 1: Just run RandomMachine.py from your terminal >> This will print duration into your console
- Use case 2: Import class from RandomMachine.py into your program and use read() function as value
- Use case 3: Use it within [FoxDot](https://foxdot.org/)

  1. Copy RandomMachine.py into /FoxDot/Custom directory
  2. Open &#95;&#95;init&#95;&#95;.py in same directory and add following lines:
  ~~~
    from .Custom.RandomMachine import RandomMachine
  ~~~
  3. Close and start FoxDot
  4. Make sure the device is on and connected to your Computer
  5. Example of use (used for video above):
  ~~~
  # Initiate RandomMachine class
  rm = RandomMachine()
  #
  # Set changable variable for duration
  var.dur1 = var([2])
  #
  # Create some random notes
  var.note = PRand(Scale.default)
  var.note2 = PRand(Scale.default, seed=9)
  #
  # Add function to update duration
  def rand_dur(dur):
      new_dur = int(rm.read()) / 1000
      if dur is new_dur:
          pass
      else:
          var.dur1 =  var([new_dur])
      dur = new_dur
  #
  # Run function loop
  Clock.every(1/8, lambda: rand_dur(dur))
  #
  # Add some synths
  #
  s1 >> sawbass(var.note2, dur=var.dur1*2, sus=var.dur1*2, amplify=1/3)
  s2 >> sitar(var.note, dur=var.dur1 / 2, amplify=7/8)
  s3 >> sitar(var.note, oct=4, dur=var.dur1, amplify=4/8)
  #
  # Close connection to device
  rm.close()
  ~~~
