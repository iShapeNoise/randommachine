const int microphonePin = A2;
bool trig_one = false;
bool trig_two = false;
unsigned long time_1 = 0;
unsigned long time_2 = 0;
unsigned long dur;

void setup() {
  Serial.begin(9600);
  Serial.setTimeout(100);
}

void loop() {
  int mn = 1024;
  int mx = 0;
  for (int i = 0; i < 1000; ++i) {
    int val = analogRead(microphonePin);
    mn = min(mn, val);
    mx = max(mx, val);
  }
  int delta = mx - mn;
  if (delta > 100){
    if (((trig_one == false) && (trig_two == false)) || (trig_two == true)){
      trig_one = true;
      trig_two = false;
      time_2 = millis();
      dur = time_2 - time_1;
      Serial.println(dur);
    }
    else if (trig_one == true){
      trig_one = false;
      trig_two = true;
      time_1 = millis();
      dur = time_1 - time_2;
      Serial.println(dur);
    }
  }
}
