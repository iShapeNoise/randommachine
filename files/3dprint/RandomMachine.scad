

//difference(){
//    cube([115, 68, 28], center=true);
//    translate([0, 0, 2])
//    cube([110, 62, 28], center=true);
//    translate([56, 2, -1.5])
//    cube([4, 10, 5], center=true);
//}

difference(){
    union(){
        cube([115, 68, 2], center=true);
        translate([20, -4, 11])
        cube([54, 60, 22], center=true);
        translate([-25, -22, 7.5])
        cube([37, 24, 13], center=true);      
    }
    union(){
        translate([20, 0, 9])
        cube([50, 50, 22], center=true);
        translate([20, 0, 13])
        cube([44, 44, 22], center=true);
        translate([-24, -22, 5.5])
        cube([34, 20, 13], center=true);
        translate([-42, -22, 5.5])
        cube([4, 10, 5], center=true); 
    }
}
