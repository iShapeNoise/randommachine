# Initiate RandomMachine class 
rm = RandomMachine()

# Set changable variable for duration
var.dur1 = var([2])

# Create some random notes
var.note = PRand(Scale.default)
var.note2 = PRand(Scale.default, seed=9)

# Add function to update duration
def rand_dur(dur):
    new_dur = int(rm.read()) / 1000
    if dur is new_dur:
        pass
    else:
        var.dur1 =  var([new_dur])
    dur = new_dur

# Run function loop
Clock.every(1/8, lambda: rand_dur(dur))

# Add some synths

s1 >> sawbass(var.note2, dur=var.dur1*2, sus=var.dur1*2, amplify=1/3)
s2 >> sitar(var.note, dur=var.dur1 / 2, amplify=7/8)
s3 >> sitar(var.note, oct=4, dur=var.dur1, amplify=4/8)

# Close connection to device
rm.close()


